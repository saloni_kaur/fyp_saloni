<?php

	require('../activities/walkingActivity.php');
	require('../activities/painActivity.php');
	require('../activities/restingActivity.php');
	require('../activities/physioActivity.php');
	require('../activities/joggingActivity.php');
	$minsTotal = $totalTime1 + $totalTime2 + $totalTime3 + $totalTime4 + $totalTime5;
	$percentageWalking = ($totalTime1 / $minsTotal) * 100;
	$percentageJogging = ($totalTime2 / $minsTotal) * 100;
	$percentageResting = ($totalTime3 / $minsTotal) * 100;
	$percentagePain = ($totalTime4 / $minsTotal) * 100;
	$percentagePhysio = ($totalTime5 / $minsTotal) * 100;

  $maxActivity = max($percentagePhysio, $percentagePain, $percentageResting, $percentageJogging, $percentageWalking);
  global $tipsForActivity;

  global $targetQuestion;
	$targetQuestion = "Which was the major activity?";
?>
<?php

    if($maxActivity == $percentageJogging){
      $tipsForActivity .= "Jogging was done the most last week - " . round($maxActivity) . "%.";
    }

    if($maxActivity == $percentageWalking){
      $tipsForActivity .= "Walking was done the most last week - " . round($maxActivity) . "%.";
    }

    if($maxActivity == $percentageResting){
       $tipsForActivity .= "Resting was done the most last week - " . round($maxActivity) . "%.";
    }

    if($maxActivity == $percentagePain){
       $tipsForActivity .= "Pain was felt the most last week - " . round($maxActivity) . "%.";
    }

    if($maxActivity == $percentagePhysio){
       $tipsForActivity .= "Physiotherapy was done the most last week - " . round($maxActivity) . "%.";
    }

?>
<!DOCTYPE html>
<html>
<style>
  text{
    font-size: 20px !important; 
  }
</style>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Activity - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">
		

		<script src="http://d3js.org/d3.v3.min.js"></script>

		<script src="http://dimplejs.org/dist/dimple.v2.1.2.min.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require_once('../topNav.php'); ?>

	<div class="container">
		<?php require_once('../weekInFocus.php'); ?> 
        <div class="row">
            <div class="col-lg-12">
                <h3 id="questions">Target Question: <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks">Remarks: <?php echo $tipsForActivity;  ?></h4>
                		<input type="submit" name="majorActivitySubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
               			<script>
							function sendToSummary(){

								var question = <?php echo json_encode($targetQuestion); ?>;
								var remarks = $('#remarks')[0].innerHTML;

								$.post( "../forSummary.php", { majorActivityquestionTag: question, majorActivityremarkTag: remarks })
								.done(function( data ) {
								    console.log(data);
								    data = eval("(" +data+ ")");
								    location.replace(data['nextPage']);
								  });
																
							}

						</script>
            </div>
        </div>
    </div>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	    	 

	        <div class="row">

	            <div class="col-md-3">
	            	<p class="lead"><u>Percentage minutes spent on each activity.</u></p>
	            </div>

	            <div class="col-md-9">

	                <div class="well">
					 <script type="text/javascript">
					      var svg = dimple.newSvg(".well", 850, 700);
					      var data = [
					        { "Activity":"Physio", "% mins This Week":<?php echo $percentagePhysio; ?> },
					        { "Activity":"Jog", "% mins This Week":<?php echo $percentageJogging; ?> },
					        { "Activity":"Pain", "% mins This Week":<?php echo $percentagePain; ?> },
					        { "Activity":"Rest", "% mins This Week":<?php echo $percentageResting; ?> },
					        { "Activity":"Walk", "% mins This Week":<?php echo $percentageWalking; ?> }
					      ];
					      var chart = new dimple.chart(svg, data);
					      chart.addCategoryAxis("x", "Activity");
					      chart.addMeasureAxis("y", "% mins This Week");
					      chart.addSeries("Activity", dimple.plot.bar);
					      chart.draw(500);
					 </script>
	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->
		    
				




    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>

