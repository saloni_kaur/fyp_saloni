<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Activity - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">

		<!-- <link type="text/css" href="http://mbostock.github.io/d3/style.css" rel="stylesheet" /> -->
		<link type="text/css" href="../css/example.css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src="../gantt-chart-d3.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		 <?php require_once('../topNav.php'); ?>

	<div class="container">

		<?php require_once('../weekInFocus.php'); ?>   

        <div class="row">
            <div class="col-lg-12">
                <h3 id="questions"><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks"><b>Remarks:</b> <?php echo $tipsForPhysio; ?></h4>
                <h4>Ideal Energy Value is (Recommended By Physiotherapist): <?php echo $idealEnergyValue; ?></h4> 
                <!-- Button trigger modal -->
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#updateEnergy">
				  Update Ideal Intensity
				</button>
				<input type="submit" name="physioIntensitySubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
               			<script>
							function sendToSummary(){

								var question = <?php echo json_encode($targetQuestion); ?>;
								var remarks = $('#remarks')[0].innerHTML;

								$.post( "../forSummary.php", { physioIntensityquestionTag: question, physioIntensityremarkTag: remarks })
								.done(function( data ) {
								    console.log(data);
								    data = eval("(" +data+ ")");
								    location.replace(data['nextPage']);
								  });
																
							}

						</script>

            </div>
        </div>
    </div>

    <!-- Modal -->
	<div class="modal fade" id="updateEnergy" tabindex="-1" role="dialog" aria-labelledby="updateEnergy" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Update Ideal Intensity</h4>
	      </div>
	      <div class="modal-body">
				<b>Name:</b><?php echo $name; ?>
				<b>Age:</b><?php echo $age; ?>
				<b>Exercise Ideal Energy:</b> <input type="text" id="energyValue" value=<?php echo json_encode($idealEnergyValue); ?>/>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary"  onclick="saveNewEnergyIdeal()">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script>
		function saveNewEnergyIdeal(){

			var newIdealValue = $('#energyValue')[0].value;

			var posting = $.post( "../saveIdealEnergyValue.php", { newIdeal: newIdealValue }, function( data ) {
				    $( "#energyValue" ).value = data;
				});
			$('#updateEnergy').modal('hide');
			window.location.reload();
		}
	</script>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	        <div class="row">

	            <div class="col-md-3">

	            	<p class="lead"><u>Physiotherapy session's intensity.</u></p>

	                <p class="lead">Legend</p>
	                <TABLE class="table table-bordered">
					   <TR>
					      <TH>Energy</TH>
					      <TH>Color Associated</TH>
					   </TR>
						<TR ALIGN="LEFT" id="overExert">
					      <TD>Overexert (Energy > <?php echo $idealEnergyValue; ?>)</TD>
					      <TD style="background-color:#FF0000;">
						      <div></div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="ideal">
					      <TD>Ideal (Energy = <?php echo $idealEnergyValue; ?>)</TD>
					      <TD style="background-color:#669900;">
						      <div> </div>
					      </TD>
					    </TR>
					     <TR ALIGN="LEFT" id="underExert">
					      <TD>Underexert (Energy &lt <?php echo $idealEnergyValue; ?>)</TD>
					      <TD style="background-color:#33b5e5;">
						      <div> </div>
					      </TD>
					    </TR>
					</TABLE>

					<?php require('../legendInstruction.php'); ?>

	            </div>

	            <div class="col-md-9">

	                <div class="well">
						<script>
								var tasks = [];
								var taskNames = [];
								var taskStatus = {};
								<?php foreach($physioEnergy as $key=>$value): ?>

									<?php if($value['value'] >= $idealEnergyValue): ?>
										<?php $countRedBar++; ?> 
										taskStatus.toomuch = "bar-pain";
										var status = "toomuch";
									<?php elseif ($value['value'] < $idealEnergyValue  && $value['value'] >= ($idealEnergyValue - 20)): ?>
										taskStatus.average = "bar-resting";
										var status = "average";
									<?php else: ?>
										<?php $countBlueBar++; ?> 
										taskStatus.toomild = "bar";
										var status = "toomild";
									<?php endif ?>

									<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

									taskNames.push(<?php echo ($value['day']); ?>);

								    tasks.push({
								     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
								     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
												"taskName": <?php echo json_encode($adash); ?>,
												"status": status,
												});
								      
								<?php endforeach ?>

								taskNames.sort(function(a, b){return a-b});

								for ( x = 0; x < taskNames.length; x++){
									if(taskNames[x] == '1'){
										taskNames[x] = 'Sunday';
									} else if(taskNames[x] == '2'){
										taskNames[x] = 'Monday';
									} else if(taskNames[x] == '3'){
										taskNames[x] = 'Tuesday';
									} else if(taskNames[x] == '4'){
										taskNames[x] = 'Wednesday';
									} else if(taskNames[x] == '5'){
										taskNames[x] = 'Thursday';
									} else if(taskNames[x] == '6'){
										taskNames[x] = 'Friday';
									} else if(taskNames[x] == '7'){
										taskNames[x] = 'Saturday';
									}
								}

							tasks.sort(function(a, b) {
							    return a.endDate - b.endDate;
							});
							var maxDate = tasks[tasks.length - 1].endDate;
							tasks.sort(function(a, b) {
							    return a.startDate - b.startDate;
							});
							var minDate = tasks[0].startDate;

							var format = "%I:%M %p";

							var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
							gantt(tasks);
						</script>

						<script type="text/javascript" src="../painSeries/activityPainLegend.js"></script>
	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->
		    
				




    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>