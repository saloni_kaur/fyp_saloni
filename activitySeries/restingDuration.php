<?php
	include('../globalsVar.php');
?>
<?php 
	require('../activities/restingActivity.php');
	require('../helpers/getDateOnly.php');

	$date1 = getDateOnly($allRestingArray[0]['start_time']);
?>
<?php
	$targetQuestion = "What times did the elderly rest?";
?>
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Activity - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">

		<!-- <link type="text/css" href="http://mbostock.github.io/d3/style.css" rel="stylesheet" /> -->
		<link type="text/css" href="../css/example.css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src="../gantt-chart-d3.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require_once('../topNav.php'); ?>

	<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3><b>Target Question:<b> <?php echo $targetQuestion; ?></h3>
				<h4 id="remarks"><b>Remarks:</b><?php echo "The average resting duration is " . round($averageRestDuration) . "mins."; ?></h4>
            	<input type="submit" name="jogDurationSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
               			<script>
							function sendToSummary(){

								var question = <?php echo json_encode($targetQuestion); ?>;
								var remarks = $('#remarks')[0].innerHTML;

								$.post( "../forSummary.php", { restDurationquestionTag: question, restDurationremarkTag: remarks })
								.done(function( data ) {
								    console.log(data);
								    data = eval("(" +data+ ")");
								    location.replace(data['nextPage']);
								  });
																
							}

						</script>
            </div>
        </div>
    </div>


	</script>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	    	<?php require_once('../weekInFocus.php'); ?>  

	        <div class="row">

	            <div class="col-md-3">
	            	<p class="lead"><u>Resting Timetable only.</u></p>
	                 <p class="lead">Legend</p>
	                <TABLE class="table table-bordered">
					   <TR>
					      <TH>Activity</TH>
					      <TH>Color Associated</TH>
					   </TR>
					    <TR ALIGN="LEFT">
					      <TD id="restingCell">Resting</TD>
					      <TD style="background-color:#669900">
					      </TD>
					    </TR>
					</TABLE>


	            </div>

	            <div class="col-md-9">

	                <div class="well">
						<script>
								var tasks = [];
								var taskNames = [];
								var taskStatus = {};
								<?php foreach($allRestingArray as $key=>$value): ?>
									taskStatus.average = "bar-resting";
										var status = "average";

										<?php switch ($value['day']) {
											case '1' :	$adash = 'Sunday'; break;
											case '2' :	$adash = 'Monday'; break;
											case '3' :	$adash = 'Tuesday'; break;
											case '4' :	$adash = 'Wednesday'; break;
											case '5' :	$adash = 'Thursday'; break;
											case '6' :	$adash = 'Friday'; break;
											case '7' :	$adash = 'Saturday'; break;
										} ?>

									taskNames.push(<?php echo ($value['day']); ?>);

								    tasks.push({
								     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
								     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
												"taskName": <?php echo json_encode($adash); ?>,
												"status": status,
												});
								      
								<?php endforeach ?>

								taskNames.sort(function(a, b){return a-b});

								for ( x = 0; x < taskNames.length; x++){
									if(taskNames[x] == '1'){
										taskNames[x] = 'Sunday';
									} else if(taskNames[x] == '2'){
										taskNames[x] = 'Monday';
									} else if(taskNames[x] == '3'){
										taskNames[x] = 'Tuesday';
									} else if(taskNames[x] == '4'){
										taskNames[x] = 'Wednesday';
									} else if(taskNames[x] == '5'){
										taskNames[x] = 'Thursday';
									} else if(taskNames[x] == '6'){
										taskNames[x] = 'Friday';
									} else if(taskNames[x] == '7'){
										taskNames[x] = 'Saturday';
									}
								}


							tasks.sort(function(a, b) {
							    return a.endDate - b.endDate;
							});
							var maxDate = tasks[tasks.length - 1].endDate;
							tasks.sort(function(a, b) {
							    return a.startDate - b.startDate;
							});
							var minDate = tasks[0].startDate;

							var format = "%H:%M";

							var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
							gantt(tasks);

							$('.bar-resting').mouseenter(function(){
								//on hover
								var icon = $('#restingCell');
								icon.attr('style', 'background-color: #C0C0C0');
							});

							$('.bar-resting').on('mouseleave', function(){
								//out of hover
								var icon = $('#restingCell');
								icon.attr('style', 'background-color: #fff');
							});
						</script>
	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>