<?php
	include('../globalsVar.php');
?>
<?php 
	require('../activities/physioEnergy.php');
	require('../helpers/getDateOnly.php');
	require ('../getElderlyParticulars.php');

	 $name = $particularsArray[0]['name'];
	 $age = $particularsArray[0]['age'];
	 $idealEnergyValue = $particularsArray[0]['idealEnergyValue'];

	$idealEnergyValue = $particularsArray[0]['idealEnergyValue'];
	$physioEnergy = $allPhysioEnergy;
	$date1 = getDateOnly($physioEnergy[0]['start_time']);

	//global $questionInTarget = "exerciseIntensity";
	
	$countRedBar = 0;
	$countBlueBar = 0;
?>
<?php
	$targetQuestion = "Is the physiotherapy exercise done with the correct intensity?";
?>
<?php 
	foreach($physioEnergy as $key=>$value){
		if($value['value'] >= $idealEnergyValue){
			$countRedBar++;
		} else if($value['value'] < $idealEnergyValue){
			$countBlueBar++;
		}
	}
	if($countBlueBar > 0) {
		$tipsForPhysio .= " Under exertion in physiotherapy may be due to lack of energy.";
	}

	if($countRedBar > 0) {
		$tipsForPhysio .= " Over exertion in physiotherapy may increase pain levels.";
	}

	if($countRedBar > 0 && $countBlueBar > 0){
		$tipsForPhysio .= " Might need to change physiotherapy.";
	}
	
?>
<?php 
	include('contentForPhysioIntensity.php');
?>


