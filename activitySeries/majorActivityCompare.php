<?php
	require('../activities/walkingActivity.php');
	require('../activities/painActivity.php');
	require('../activities/restingActivity.php');
	require('../activities/physioActivity.php');
	require('../activities/joggingActivity.php');
	$minsTotal = $totalTime1 + $totalTime2 + $totalTime3 + $totalTime4 + $totalTime5;
	$percentageWalking = ($totalTime1 / $minsTotal) * 100;
	$percentageJogging = ($totalTime2 / $minsTotal) * 100;
	$percentageResting = ($totalTime3 / $minsTotal) * 100;
	$percentagePain = ($totalTime4 / $minsTotal) * 100;
	$percentagePhysio = ($totalTime5 / $minsTotal) * 100;

	  require('../activities/lastWalkingActivity.php');
	  require('../activities/lastPainActivity.php');
	  require('../activities/lastRestingActivity.php');
	  require('../activities/lastPhysioActivity.php');
	  require('../activities/lastJoggingActivity.php');
	  $minsTotalLast = $totalTimeLast1 + $totalTimeLast2 + $totalTimeLast3 + $totalTimeLast4 + $totalTimeLast5;
	  $percentageWalkingLast = ($totalTimeLast1 / $minsTotalLast) * 100;
	  $percentageJoggingLast = ($totalTimeLast2 / $minsTotalLast) * 100;
	  $percentageRestingLast = ($totalTimeLast3 / $minsTotalLast) * 100;
	  $percentagePainLast = ($totalTimeLast4 / $minsTotalLast) * 100;
	  $percentagePhysioLast = ($totalTimeLast5 / $minsTotalLast) * 100;

	  $maxCompare = max(abs($percentagePhysio - $percentagePhysioLast), abs($percentageWalking - $percentageWalkingLast),
	    abs($percentageJogging - $percentageJoggingLast), abs($percentagePainLast - $percentagePain), abs($percentageResting - $percentageRestingLast));
	  global $tipsForCompare;

	  global $targetQuestion;
		$targetQuestion = "What is the difference in activities duration in the last 2 weeks?";
?>
<?php

    if(abs($percentageJogging - $percentageJoggingLast) == $maxCompare){
      $tipsForCompare .= "The highest difference was in Jogging.";

      if($percentageJoggingLast - $percentageJogging < 0){
      	$tipsForCompare .= "There was an increase in the latest week.";
      } else {
      	$tipsForCompare .= "There was a decrease in the latest week.";
      }
    }

    if(abs($percentagePainLast - $percentagePain) == $maxCompare){
      $tipsForCompare .= "The highest difference was in Pain occurence.";

      if($percentagePainLast - $percentagePain < 0){
      	$tipsForCompare .= "There was an increase in the latest week.";
      } else {
      	$tipsForCompare .= "There was a decrease in the latest week.";
      }
    }

    if(abs($percentageResting - $percentageRestingLast) == $maxCompare){
      $tipsForCompare .= "The highest difference was in Resting.";

      if($percentageRestingLast - $percentageResting < 0){
      	$tipsForCompare .= "There was an increase in the latest week.";
      } else {
      	$tipsForCompare .= "There was a decrease in the latest week.";
      }
    }
  
    if(abs($percentagePhysio - $percentagePhysioLast) == $maxCompare){
      $tipsForCompare .= "The highest difference was in Physiotherapy.";

      if($percentagePhysioLast - $percentagePhysio < 0){
      	$tipsForCompare .= "There was an increase in the latest week.";
      } else {
      	$tipsForCompare .= "There was a decrease in the latest week.";
      }
    }
    
    if(abs($percentageWalking - $percentageWalkingLast) == $maxCompare){
      $tipsForCompare .= "The highest difference was in Walking.";

      if($percentageWalkingLast - $percentageWalking < 0){
      	$tipsForCompare .= "There was an increase in the latest week.";
      } else {
      	$tipsForCompare .= "There was a decrease in the latest week.";
      }
    }
?>
<!DOCTYPE html>
<html>
<style>
  text{
    font-size: 20px !important; 
  }
</style>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Activity - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">
		

		<script src="http://d3js.org/d3.v3.min.js"></script>

		<script src="http://dimplejs.org/dist/dimple.v2.1.2.min.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require_once('../topNav.php'); ?>

	<div class="container">

		<?php require_once('../weekInFocus.php'); ?>  
		
        <div class="row">
            <div class="col-lg-12">
                <h3 id="questions"><b>Target Question:</b> <?php echo $targetQuestion;  ?></h3>
                <h4 id="remarks"><b>Remarks:</b> <?php echo $tipsForCompare; ?></h4>
                <input type="submit" name="majorCompareLevelSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
                        <script>
                            function sendToSummary(){

                                var question = <?php echo json_encode($targetQuestion); ?>;
                                var remarks = $('#remarks')[0].innerHTML;

                                $.post( "../forSummary.php", { majorComparequestionTag: question, majorCompareremarkTag: remarks })
                                .done(function( data ) {
                                    console.log(data);
                                    data = eval("(" +data+ ")");
                                    location.replace(data['nextPage']);
                                  });
                            }

                        </script>
            </div>
        </div>
    </div>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	        <div class="row">

	            <div class="col-md-3">
	            	<p class="lead"><u>Comparison of percentage minutes spent on each activity across 2 weeks.</u></p>
	            </div>

	            <div class="col-md-9">

	                <div class="well">
					<script type="text/javascript">
					      var svg = dimple.newSvg(".well", 850, 700);
					      var data = [
					        { "Activity":"Physio", "Week":"Latest Week", "% mins per Week":<?php echo $percentagePhysio; ?>},
					        { "Activity":"Jog", "Week":"Latest Week", "% mins per Week":<?php echo $percentageJogging; ?> },
					        { "Activity":"Pain", "Week":"Latest Week", "% mins per Week":<?php echo $percentagePain; ?> },
					        { "Activity":"Rest", "Week":"Latest Week", "% mins per Week":<?php echo $percentageResting; ?> },
					        { "Activity":"Walk", "Week":"Latest Week", "% mins per Week":<?php echo $percentageWalking; ?> },

					        { "Activity":"Physio", "Week":"2 weeks ago", "% mins per Week":<?php echo $percentagePhysioLast; ?>},
					        { "Activity":"Jog", "Week":"2 weeks ago", "% mins per Week":<?php echo $percentageJoggingLast; ?> },
					        { "Activity":"Pain", "Week":"2 weeks ago", "% mins per Week":<?php echo $percentagePainLast; ?> },
					        { "Activity":"Rest", "Week":"2 weeks ago", "% mins per Week":<?php echo $percentageRestingLast; ?> },
					        { "Activity":"Walk", "Week":"2 weeks ago", "% mins per Week":<?php echo $percentageWalkingLast; ?> }
					      ];
					      var chart = new dimple.chart(svg, data);
					      chart.addCategoryAxis("x", ["Activity", "Week"]);
					      chart.addMeasureAxis("y", "% mins per Week");
					      chart.addSeries("Week", dimple.plot.bar);
					      chart.addLegend(65, 10, 510, 20, "right");
					      chart.draw(500);
   					 </script>
	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->
		    
				




    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>

