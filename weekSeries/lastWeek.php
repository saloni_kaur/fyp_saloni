<?php
	require('../getActivitiesForLatestWeek.php');
	require('../helpers/getDateOnly.php');

	$date1 = getDateOnly($allActivitiesArray1[0]['start_time']);
	$date2 = getDateOnly($allActivitiesArray2[0]['start_time']);
	$date3 = getDateOnly($allActivitiesArray3[0]['start_time']);
	$date4 = getDateOnly($allActivitiesArray4[0]['start_time']);
	$date5 = getDateOnly($allActivitiesArray5[0]['start_time']);
	$date6 = getDateOnly($allActivitiesArray6[0]['start_time']);
	$date7 = getDateOnly($allActivitiesArray7[0]['start_time']);

	global $targetQuestion;
	$targetQuestion = "What was the physical activities routine like for the elderly last week?";

?>
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Gantt Chart Latest Week</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">

		<!-- <link type="text/css" href="http://mbostock.github.io/d3/style.css" rel="stylesheet" /> -->
		<link type="text/css" href="../css/example.css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src="../gantt-chart-d3.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require('../topNav.php'); ?>

	    	 

	<div class="container">
		<?php require('../weekInFocus.php'); ?>
        <div class="row">
            <div class="col-lg-12">
                <h3><b>Target Question:</b> <?php echo $targetQuestion;?></h3>
            </div>
        </div>
    </div>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	        <div class="row">

	            <div class="col-md-3">
	               

	                <p class="lead">Legend</p>
	                <TABLE class="table table-bordered">
					   <TR>
					      <TH>Activity</TH>
					      <TH>Color Associated</TH>
					   </TR>
						<TR ALIGN="LEFT" id="walkingCell">
					      <TD>Walking</TD>
					      <TD style="background-color:#33b5e5">
						      <div></div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="joggingCell">
					      <TD>Jogging</TD>
					      <TD style="background-color:#800080">
						      <div> </div>
					      </TD>
					    </TR>
					     <TR ALIGN="LEFT" id="restingCell">
					      <TD>Resting</TD>
					      <TD style="background-color:#669900">
						      <div> </div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="physioCell">
					      <TD>Phyiotherapy</TD>
					      <TD style="background-color:#ffbb33">
						      <div> </div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="painCell">
					      <TD>Pain</TD>
					      <TD style="background-color:#FF0000">
						      <div> </div>
					      </TD>
					    </TR>
					</TABLE>

	            </div>

	            <div class="col-md-9">

	                <div class="well">

	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->
		    
				<script>
					var tasks = [];
					<?php foreach($allActivitiesArray1 as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>


					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode($value['start_time']); ?>),
					     			"endDate": new Date(<?php echo json_encode($value['end_time']); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					      
					<?php endforeach ?>

					<?php foreach($allActivitiesArray2 as $key=>$value): ?>
						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>

					<?php foreach($allActivitiesArray3 as $key=>$value): ?>
						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>

					<?php foreach($allActivitiesArray4 as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>

					<?php foreach($allActivitiesArray5 as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>
							
					<?php foreach($allActivitiesArray6 as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>
							
					<?php foreach($allActivitiesArray7 as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

					     tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": <?php echo json_encode($value['description']); ?>
									});
					<?php endforeach ?>

				var taskStatus = {
				    "walking" : "bar",
				    "pain" : "bar-pain",
				    "resting" : "bar-resting",
				    "physiotherapy" : "bar-physio",
				    "jogging" : "bar-jogging"
				};

				var taskNames = [];
				taskNames.push(<?php echo json_encode("Sunday"); ?>);
				taskNames.push(<?php echo json_encode("Monday"); ?>);
				taskNames.push(<?php echo json_encode("Tuesday"); ?>);
				taskNames.push(<?php echo json_encode("Wednesday"); ?>);
				taskNames.push(<?php echo json_encode("Thursday"); ?>);
				taskNames.push(<?php echo json_encode("Friday"); ?>);
				taskNames.push(<?php echo json_encode("Saturday"); ?>);

				tasks.sort(function(a, b) {
				    return a.endDate - b.endDate;
				});
				var maxDate = tasks[tasks.length - 1].endDate;
				tasks.sort(function(a, b) {
				    return a.startDate - b.startDate;
				});
				var minDate = tasks[0].startDate;

				var format = "%I %p";

				var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
				gantt(tasks);

				$('.bar').mouseenter(function(){
					//on hover
					var icon = $('#walkingCell');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar').on('mouseleave', function(){
					//out of hover
					var icon = $('#walkingCell');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-jogging').mouseenter(function(){
					//on hover
					var icon = $('#joggingCell');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-jogging').on('mouseleave', function(){
					//out of hover
					var icon = $('#joggingCell');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-pain').mouseenter(function(){
					//on hover
					var icon = $('#painCell');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-pain').on('mouseleave', function(){
					//out of hover
					var icon = $('#painCell');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-resting').mouseenter(function(){
					//on hover
					var icon = $('#restingCell');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-resting').on('mouseleave', function(){
					//out of hover
					var icon = $('#restingCell');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-physio').mouseenter(function(){
					//on hover
					var icon = $('#exercisingCell');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-physio').on('mouseleave', function(){
					//out of hover
					var icon = $('#exercisingCell');
					icon.attr('style', 'background-color: #fff');
				});


				$('#walkingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #E6E6E6');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #E6E6E6');
				});

				$('#walkingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #800080');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #ffbb33');
				});

				$('#restingCell').mouseenter(function(){
					//on hover
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #E6E6E6');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #E6E6E6');
				});

				$('#restingCell').on('mouseleave', function(){
					//out of hover
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #800080');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #ffbb33');
				});

				$('#joggingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #E6E6E6');
				});

				$('#joggingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #ffbb33');
				});

				$('#physioCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #E6E6E6');
				});

				$('#physioCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #800080');
				});

				$('#painCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #E6E6E6');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #E6E6E6');
				});

				$('#painCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
					var bar4 = $('.bar-jogging');
					bar4.attr('style', 'fill: #800080');
					var bar5 = $('.bar-physio');
					bar5.attr('style', 'fill: #ffbb33');
				});

			</script>

			<!-- <div id="iconActivity"></div>
			<div id="visualizationTitle">Graph Title:<b>Latest Week's Physical Activities' Routine</b></div> -->




    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>

