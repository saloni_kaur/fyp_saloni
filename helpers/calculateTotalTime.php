<?php
	function calculateTotalTime($allArray){

		$interval = 0;
		foreach ($allArray as $i){
			list($year1, $month1, $day1, $hour1, $min1, $sec1) = preg_split('/[: -]/', $i['start_time']);
			list($year2, $month2, $day2, $hour2, $min2, $sec2) = preg_split('/[: -]/', $i['end_time']);

			if($min1 < $min2){
				$interval += ($hour2 - $hour1) * 60 + ($min2 - $min1);
			} else if($min1 > $min2){
				if($hour2 - $hour1 == 1){
					$interval += 60 - ($min1 - $min2);
				} else {
					$interval += ($hour2 - $hour1 - 1) * 60 + (60 - ($min1 - $min2));
				}
				
			} else{
				$interval += ($hour2 - $hour1) * 60;
			}
		}
		return $interval;
	}

	function calculateAvgTime($allArray){

		$interval = 0;
		$count = 0;
		foreach ($allArray as $i){
			list($year1, $month1, $day1, $hour1, $min1, $sec1) = preg_split('/[: -]/', $i['start_time']);
			list($year2, $month2, $day2, $hour2, $min2, $sec2) = preg_split('/[: -]/', $i['end_time']);

			if($min1 < $min2){
				$interval += ($hour2 - $hour1) * 60 + ($min2 - $min1);
				$count++;
			} else if($min1 > $min2){
				if($hour2 - $hour1 == 1){
					$interval += 60 - ($min1 - $min2);
				} else {
					$interval += ($hour2 - $hour1 - 1) * 60 + (60 - ($min1 - $min2));
				}
				$count++;
			} else{
				$interval += ($hour2 - $hour1) * 60;
				$count++;
			}
		}

		$average = $interval / $count;

		return $average;
	}
?>