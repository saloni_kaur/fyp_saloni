<?php
	
	$painPhysio = 0;
	$painJogging = 0;
	$painResting = 0;
	$painWalking = 0;

	$countPhysio = 0;
	$countJogging = 0;
	$countWalking = 0;
	$countResting = 0;

	$resultantArray1 = array();
	$resultantArray2 = array();
	$resultantArray3 = array();
	$resultantArray4 = array();


	for ($x = 0; $x < count($allBeforePainArray); $x++){

		if($allBeforePainArray[$x]['description'] == "physiotherapy"){
			$painPhysio += (int)$painLevelArray[$x];
			$countPhysio++;
			$resultantArray1 = array('Physiotherapy' => $painPhysio);

		} else if($allBeforePainArray[$x]['description'] == "jogging"){
			$painJogging += (int)$painLevelArray[$x];
			$countJogging++;
			$resultantArray2 = array('Jogging' => $painJogging);

		} else if($allBeforePainArray[$x]['description'] == "resting"){
			$painResting += (int)$painLevelArray[$x];
			$countResting++;
			$resultantArray3 = array('Resting' => $painResting);

		} else if($allBeforePainArray[$x]['description'] == "walking"){
			$painWalking += (int)$painLevelArray[$x];
			$countWalking++;
			$resultantArray4 = array('Walking' => $painWalking);
		}
	}


	//make an array of the intial arrays
	$arrs = array();
	$arrs[] = $resultantArray1;
	$arrs[] = $resultantArray2;
	$arrs[] = $resultantArray3;
	$arrs[] = $resultantArray4;

	$resultantArray = array();

	foreach($arrs as $arr){
		if(is_array($arr)){
			$resultantArray = array_merge($resultantArray, $arr);
		}
	}

	//print json_encode($resultantArray);
	
?>