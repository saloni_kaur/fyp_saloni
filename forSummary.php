<?php
$link = mysqli_connect('localhost', 'root', 'silversense123');
mysqli_select_db($link,'silversense');
// Evaluate the connection
if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
    exit();
} else {
	//echo "Successful database connection, happy coding!!!";
}
?>
<?php 
	if(isset($_POST['physioPainquestionTag'])){
		$physioPainquestionTag = $_POST['physioPainquestionTag'];
		$physioPainremarkTag = $_POST['physioPainremarkTag'];

		$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=1 AND numOfVisitedPages > 0");
		$rowCount2 = mysqli_num_rows($sqlQuery2);

		//check if numOfVisitedPages till now
		if($rowCount2 > 0){
			while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
				$numOfVisitedPages = $row2['numOfVisitedPages'];
			}
		} else {
			//if none than start with one.
			$numOfVisitedPages = 0;
		}

		if($numOfVisitedPages == 2){
			$nextPage = "../reportCard.php";
		} else {
			$nextPage = "beforePainLevelTable.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($physioPainquestionTag) . 
				",1,1,1," . json_encode($physioPainremarkTag) . "," . $numOfVisitedPages .")");
			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $physioPainquestionTag, 'remark' => $physioPainremarkTag, 'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		
	}


	if(isset($_POST['beforePainLevelquestionTag'])){
		$beforePainLevelquestionTag = $_POST['beforePainLevelquestionTag'];
		$beforePainLevelremarkTag = $_POST['beforePainLevelremarkTag'];


		$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=2 AND numOfVisitedPages > 0");
		$rowCount2 = mysqli_num_rows($sqlQuery2);

		//check if numOfVisitedPages till now
		if($rowCount2 > 0){
			while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
				$numOfVisitedPages = $row2['numOfVisitedPages'];
			}
		} else {
			//if none than start with one.
			$numOfVisitedPages = 0;
		}

		if($numOfVisitedPages == 2){
			$nextPage = "../reportCard.php";
		} else {

			$beforePainActivity = preg_split('/\s+/', $beforePainLevelremarkTag)[1];

			switch($beforePainActivity){
				case "Physiotherapy": $nextPage = "physioPain.php"; break;
				case "Resting": $nextPage = "restingPain.php"; break;
				case "Walking": $nextPage = "walkingPain.php"; break;
				case "Jogging": $nextPage = "joggingPain.php"; break;
				default: $nextPage = "../reportCard.php";
			}

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($beforePainLevelquestionTag) . 
				",1,1,2," . json_encode($beforePainLevelremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $beforePainLevelquestionTag, 'remark' => $beforePainLevelremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}
	}

		if(isset($_POST['beforePainTablequestionTag'])){
			$beforePainTablequestionTag = $_POST['beforePainTablequestionTag'];
			$beforePainTableremarkTag = $_POST['beforePainTableremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=3 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			if($numOfVisitedPages == 2){
				$nextPage = "../reportCard.php";
			} else {

				$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

				switch($beforePainActivity){
					case "Physiotherapy": $nextPage = "physioPain.php"; break;
					case "Resting": $nextPage = "restingPain.php"; break;
					case "Walking": $nextPage = "walkingPain.php"; break;
					case "Jogging": $nextPage = "joggingPain.php"; break;
					default: $nextPage = "../reportCard.php";
				}
			}
		

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($beforePainTablequestionTag) . 
				",1,1,3," . json_encode($beforePainTableremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $beforePainTablequestionTag, 'remark' => $beforePainTableremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['restingPainquestionTag'])){
			$restingPainquestionTag = $_POST['restingPainquestionTag'];
			$restingPainremarkTag = $_POST['restingPainremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=4 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			$nextPage = "../reportCard.php";
			// if($numOfVisitedPages == 2){
			// 	$nextPage = "../reportCard.php";
			// } else {

			// 	$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

			// 	switch($beforePainActivity){
			// 		case "Physiotherapy": $nextPage = "physioPain.php"; break;
			// 		case "Resting": $nextPage = "restingPain.php"; break;
			// 		case "Walking": $nextPage = "walkingPain.php"; break;
			// 		case "Jogging": $nextPage = "joggingPain.php"; break;
			// 		default: $nextPage = "../reportCard.php";
			// 	}
			// }
		

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($restingPainquestionTag) . 
				",1,1,4," . json_encode($restingPainremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $restingPainquestionTag, 'remark' => $restingPainremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['painDurationquestionTag'])){
			$painDurationquestionTag = $_POST['painDurationquestionTag'];
			$painDurationremarkTag = $_POST['painDurationremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=5 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			// if($numOfVisitedPages == 2){
			// 	$nextPage = "../reportCard.php";
			// } else {

			// 	$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

			// 	switch($beforePainActivity){
			// 		case "Physiotherapy": $nextPage = "physioPain.php"; break;
			// 		case "Resting": $nextPage = "restingPain.php"; break;
			// 		case "Walking": $nextPage = "walkingPain.php"; break;
			// 		case "Jogging": $nextPage = "joggingPain.php"; break;
			// 		default: $nextPage = "../reportCard.php";
			// 	}
			// }
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($painDurationquestionTag) . 
				",1,1,5," . json_encode($painDurationremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $painDurationquestionTag, 'remark' => $painDurationremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['walkingPainquestionTag'])){
			$walkingPainquestionTag = $_POST['walkingPainquestionTag'];
			$walkingPainremarkTag = $_POST['walkingPainremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=6 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			// if($numOfVisitedPages == 2){
			// 	$nextPage = "../reportCard.php";
			// } else {

			// 	$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

			// 	switch($beforePainActivity){
			// 		case "Physiotherapy": $nextPage = "physioPain.php"; break;
			// 		case "Resting": $nextPage = "restingPain.php"; break;
			// 		case "Walking": $nextPage = "walkingPain.php"; break;
			// 		case "Jogging": $nextPage = "joggingPain.php"; break;
			// 		default: $nextPage = "../reportCard.php";
			// 	}
			// }
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($walkingPainquestionTag) . 
				",1,1,6," . json_encode($walkingPainremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $walkingPainquestionTag, 'remark' => $walkingPainremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['joggingPainquestionTag'])){
			$joggingPainquestionTag = $_POST['joggingPainquestionTag'];
			$joggingPainremarkTag = $_POST['joggingPainremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=7 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			// if($numOfVisitedPages == 2){
			// 	$nextPage = "../reportCard.php";
			// } else {

			// 	$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

			// 	switch($beforePainActivity){
			// 		case "Physiotherapy": $nextPage = "physioPain.php"; break;
			// 		case "Resting": $nextPage = "restingPain.php"; break;
			// 		case "Walking": $nextPage = "walkingPain.php"; break;
			// 		case "Jogging": $nextPage = "joggingPain.php"; break;
			// 		default: $nextPage = "../reportCard.php";
			// 	}
			// }
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($joggingPainquestionTag) . 
				",1,1,7," . json_encode($joggingPainremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $joggingPainquestionTag, 'remark' => $joggingPainremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['physioIntensityquestionTag'])){
			$physioIntensityquestionTag = $_POST['physioIntensityquestionTag'];
			$physioIntensityremarkTag = $_POST['physioIntensityremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=10 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}

			// if($numOfVisitedPages == 2){
			// 	$nextPage = "../reportCard.php";
			// } else {

			// 	$beforePainActivity = preg_split('/\s+/', $beforePainTableremarkTag)[1];

			// 	switch($beforePainActivity){
			// 		case "Physiotherapy": $nextPage = "physioPain.php"; break;
			// 		case "Resting": $nextPage = "restingPain.php"; break;
			// 		case "Walking": $nextPage = "walkingPain.php"; break;
			// 		case "Jogging": $nextPage = "joggingPain.php"; break;
			// 		default: $nextPage = "../reportCard.php";
			// 	}
			// }
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($physioIntensityquestionTag) . 
				",1,1,10," . json_encode($physioIntensityremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $physioIntensityquestionTag, 'remark' => $physioIntensityremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['majorActivityquestionTag'])){
			$majorActivityquestionTag = $_POST['majorActivityquestionTag'];
			$majorActivityremarkTag = $_POST['majorActivityremarkTag'];


			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=8 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($majorActivityquestionTag) . 
				",1,1,8," . json_encode($majorActivityremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $majorActivityquestionTag, 'remark' => $majorActivityremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['majorComparequestionTag'])){
			$majorComparequestionTag = $_POST['majorComparequestionTag'];
			$majorCompareremarkTag = $_POST['majorCompareremarkTag'];

			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=9 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}
			
			if($numOfVisitedPages >= 2){
				$nextPage = "../reportCard.php";
			} else {

				$nextPage = "#";
			}

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($majorComparequestionTag) . 
				",1,1,9," . json_encode($majorCompareremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $majorComparequestionTag, 'remark' => $majorCompareremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}

		if(isset($_POST['jogDurationquestionTag'])){
			$jogDurationquestionTag = $_POST['jogDurationquestionTag'];
			$jogDurationremarkTag = $_POST['jogDurationremarkTag'];

			$sqlQuery2 = mysqli_query($link, "SELECT numOfVisitedPages FROM report WHERE elderly_id=1 AND user_id=1 AND question_id=11 AND numOfVisitedPages > 0");
			$rowCount2 = mysqli_num_rows($sqlQuery2);

			//check if numOfVisitedPages till now
			if($rowCount2 > 0){
				while($row2 = mysqli_fetch_array($sqlQuery2, MYSQLI_ASSOC)){
					$numOfVisitedPages = $row2['numOfVisitedPages'];
				}
			} else {
				//if none than start with one.
				$numOfVisitedPages = 0;
			}
			
			$nextPage = "../reportCard.php";

			$numOfVisitedPages++;
		
			$sqlQuery1 = mysqli_query($link, "INSERT INTO report (question, elderly_id, user_id, question_id, remark, numOfVisitedPages) VALUES (" . json_encode($jogDurationquestionTag) . 
				",1,1,11," . json_encode($jogDurationremarkTag) . "," . $numOfVisitedPages .")");

			echo json_encode(array('returned_val' => $sqlQuery1, 'question' => $jogDurationquestionTag, 'remark' => $jogDurationremarkTag,  'numOfVisitedPages' => $numOfVisitedPages, 'nextPage' => $nextPage));
		}
		
	
?>