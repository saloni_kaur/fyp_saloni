<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pain - Specific Category</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require_once('topNav-root.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <?php require_once('weekInFocus.php'); ?>                                              

        <div class="row">

           <div class="col-md-3">
                <p class="lead">Categories</p>
                <div class="list-group">
                    <a href="weekSpecific.php" class="list-group-item">Week -Specific</a>
                    <a href="painSpecific.php" class="list-group-item active">Pain - Specific</a>
                    <a href="activitySpecific.php" class="list-group-item">Activity - Specific</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="well">

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/beforePainTable.php">Pain occurred the most after which physical activities?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/beforePainLevelTable.php">The total pain level (all ranges) was the highest after which physical activities?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/painDuration.php">What was the average duration of the pain?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/painlocation.php">Where is it the most painful?</a> 
                            
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/timesPain.php">What times did the elderly have pain?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/restingPain.php">Did resting cause a pain level of more than 7?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/walkingPain.php">Did walking cause a pain level of more than 7?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/joggingPain.php">Did jogging cause a pain level of more than 7?</a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="painSeries/physioPain.php">Did physiotherapy cause a pain level of more than 7?</a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

  

    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
