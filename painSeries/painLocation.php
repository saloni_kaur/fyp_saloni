<?php
    require ('../activities/painActivity.php');

   global $targetQuestion;
   $targetQuestion = "Where is it the most painful?";
   $tipsForPain = "";
?>  
<?php 

    $countBack = 0;
    $countKnee = 0;
    $countHead = 0;

    foreach($allPainArray as $key=>$value){
        if($value['painLocation'] == "back"){
            $countBack++;
        } 
        if($value['painLocation'] == "knee"){
            $countKnee++;
        }
        if($value['painLocation'] == "head"){
            $countHead++;
        }
    }

    $maxPain = max($countHead, $countKnee, $countBack);

    $arrayPainLocation = array();
    foreach($allPainArray as $key=>$value){
        if(!in_array($value['day'], $arrayPainLocation)){
            array_push($allPainArray, $value['day']);
        }
    }
    
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pain - Specific Category</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-item.css" rel="stylesheet">

     <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</head>

<body>

    <?php require_once('../topNav.php'); ?>

     <div class="container">

        <?php require_once('../weekInFocus.php'); ?>  

        <div class="row">
            <div class="col-lg-12">
                <h3><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks"><b>Remarks:</b> <?php echo $tipsForPain; ?></h4>
                <input type="submit" name="painLocationSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
                        <script>
                            function sendToSummary(){

                                var question = <?php echo json_encode($targetQuestion); ?>;
                                var remarks = $('#remarks')[0].innerHTML;

                                $.post( "../forSummary.php", { painLocationquestionTag: question, painLocationremarkTag: remarks })
                                .done(function( data ) {
                                    console.log(data);
                                    data = eval("(" +data+ ")");
                                    location.replace(data['nextPage']);
                                  });
                            }

                        </script>
            </div>
        </div>
    </div>

    <hr>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead"><u>Pain Location by Day.</u></p>
            </div>

            <div class="col-md-9">

                <div class="well">
                    <TABLE class="table table-bordered">
                       <TR>
                          <TH>Day</TH>
                          <TH>Pain Location</TH>
                       </TR>
                       <?php foreach($allPainArray as $key=>$value): ?>

                           <?php switch ($value['day']) {
                                                case '1' :  $adash = 'Sunday'; break;
                                                case '2' :  $adash = 'Monday'; break;
                                                case '3' :  $adash = 'Tuesday'; break;
                                                case '4' :  $adash = 'Wednesday'; break;
                                                case '5' :  $adash = 'Thursday'; break;
                                                case '6' :  $adash = 'Friday'; break;
                                                case '7' :  $adash = 'Saturday'; break;
                                            } ?>

                               <TR ALIGN="LEFT">
                                  <TD><?php echo $adash; ?></TD>
                                  <TD>
                                    <?php 
                                        if($value['painLocation'] == "back"){
                                            echo "<p><img src='../images/painIcon.png' width='50px'>Back</p>"; 
                                        } 
                                        if($value['painLocation'] == "knee"){
                                            echo "<p><img src='../images/kneePain.png' width='50px'>Knee</p>"; 
                                        }
                                        if($value['painLocation'] == "head"){
                                             echo "<p><img src='../images/headPain.png' width='50px'>Head</p>"; 
                                        }
                                    ?>

                                     
                                  </TD>
                               </TR>
                            <?php endforeach ?>
                    </TABLE>
                    

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->



     <div class="container"> 

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

</body>

</html>


