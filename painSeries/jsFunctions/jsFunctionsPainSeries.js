function evaluateTipsPhysioPain(tasks){
	var sundayArr = [];
	var mondayArr = [];
	var tuesdayArr = [];
	var wednesdayArr = [];
	var thursdayArr = [];
	var fridayArr = [];
	var saturdayArr = [];

	//group in arrays according to days
	for (var i = 0; i <= tasks.length - 1; i++) {
		switch (tasks[i].taskName){
			case "Sunday": sundayArr.push(tasks[i]); break;
			case "Monday": mondayArr.push(tasks[i]); break;
			case "Tuesday": tuesdayArr.push(tasks[i]); break;
			case "Wednesday": wednesdayArr.push(tasks[i]); break;
			case "Thursday": thursdayArr.push(tasks[i]); break;
			case "Friday": fridayArr.push(tasks[i]); break;
			case "Saturday": saturdayArr.push(tasks[i]); break;
		}
	};


	//add the day arrays into another array
	var daysArr = [sundayArr, mondayArr, tuesdayArr, wednesdayArr, thursdayArr, fridayArr, saturdayArr];

	var countPhysioPain = 0;

	//sort the days array by date
	//for everyday check if there is too much pain after physio, count it
	if(daysArr.length > 0){
		for (var d = 0; d <= daysArr.length - 1; d++) {
			if(daysArr[d].length > 0){
				daysArr[d].sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return new Date(b.startDate) - new Date(a.startDate);
				});
				for (var x = 0; x < daysArr[d].length; x++) {
					if(daysArr[d][x].status == "physio"){
						var y = x+1;
						if(daysArr[d].length > y){
							if(daysArr[d][y].status == "toomuch"){
							countPhysioPain++;
							}
						}
					}
				};
			}
		};
	}

	return countPhysioPain;
}

function evaluateTipsRestPain(tasks){
	var sundayArr = [];
	var mondayArr = [];
	var tuesdayArr = [];
	var wednesdayArr = [];
	var thursdayArr = [];
	var fridayArr = [];
	var saturdayArr = [];

	//group in arrays according to days
	for (var i = 0; i <= tasks.length - 1; i++) {
		switch (tasks[i].taskName){
			case "Sunday": sundayArr.push(tasks[i]); break;
			case "Monday": mondayArr.push(tasks[i]); break;
			case "Tuesday": tuesdayArr.push(tasks[i]); break;
			case "Wednesday": wednesdayArr.push(tasks[i]); break;
			case "Thursday": thursdayArr.push(tasks[i]); break;
			case "Friday": fridayArr.push(tasks[i]); break;
			case "Saturday": saturdayArr.push(tasks[i]); break;
		}
	};


	//add the day arrays into another array
	var daysArr = [sundayArr, mondayArr, tuesdayArr, wednesdayArr, thursdayArr, fridayArr, saturdayArr];

	var countRestPain = 0;

	//sort the days array by date
	//for everyday check if there is too much pain after physio, count it
	if(daysArr.length > 0){
		for (var d = 0; d <= daysArr.length - 1; d++) {
			if(daysArr[d].length > 0){
				daysArr[d].sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return (a.startDate).getTime() - (b.startDate).getTime();
				});
				for (var x = 0; x < daysArr[d].length; x++) {
					if(daysArr[d][x].status == "resting"){
						var y = x+1;
						if(daysArr[d].length > y){
							if(daysArr[d][y].status == "toomuch"){
							countRestPain++;
							}
						}
					}
				};
			}
		};
	}

	return countRestPain;
}

function evaluateTipsWalkPain(tasks){
	var sundayArr = [];
	var mondayArr = [];
	var tuesdayArr = [];
	var wednesdayArr = [];
	var thursdayArr = [];
	var fridayArr = [];
	var saturdayArr = [];

	//group in arrays according to days
	for (var i = 0; i <= tasks.length - 1; i++) {
		switch (tasks[i].taskName){
			case "Sunday": sundayArr.push(tasks[i]); break;
			case "Monday": mondayArr.push(tasks[i]); break;
			case "Tuesday": tuesdayArr.push(tasks[i]); break;
			case "Wednesday": wednesdayArr.push(tasks[i]); break;
			case "Thursday": thursdayArr.push(tasks[i]); break;
			case "Friday": fridayArr.push(tasks[i]); break;
			case "Saturday": saturdayArr.push(tasks[i]); break;
		}
	};


	//add the day arrays into another array
	var daysArr = [sundayArr, mondayArr, tuesdayArr, wednesdayArr, thursdayArr, fridayArr, saturdayArr];

	var countWalkPain = 0;

	//sort the days array by date
	//for everyday check if there is too much pain after physio, count it
	if(daysArr.length > 0){
		for (var d = 0; d <= daysArr.length - 1; d++) {
			if(daysArr[d].length > 0){
				daysArr[d].sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return (a.startDate).getTime() - (b.startDate).getTime();
				});
				for (var x = 0; x < daysArr[d].length; x++) {
					if(daysArr[d][x].status == "resting"){
						var y = x+1;
						if(daysArr[d].length > y){
							if(daysArr[d][y].status == "toomuch"){
							countWalkPain++;
							}
						}
					}
				};
			}
		};
	}

	return countWalkPain;
}