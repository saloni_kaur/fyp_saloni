
				$('.bar').mouseenter(function(){
					//on hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #C0C0C0');
					var energy1 = $('#underExert');
					energy1.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar').on('mouseleave', function(){
					//out of hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #fff');
					var energy1 = $('#underExert');
					energy1.attr('style', 'background-color: #fff');
				});


				$('.bar-resting').mouseenter(function(){
					//on hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #C0C0C0');
					var energy1 = $('#ideal');
					energy1.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-resting').on('mouseleave', function(){
					//out of hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #fff');
					var energy1 = $('#ideal');
					energy1.attr('style', 'background-color: #fff');
				});

				$('.bar-pain').mouseenter(function(){
					//on hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #C0C0C0');
					var energy1 = $('#overExert');
					energy1.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-pain').on('mouseleave', function(){
					//out of hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #fff');
					var energy1 = $('#overExert');
					energy1.attr('style', 'background-color: #fff');
				});

				$('.bar-jogging').mouseenter(function(){
					//on hover
					var icon = $('#walkingCell');
					icon.attr('style', 'background-color: #C0C0C0');
					var icon1 = $('#physioCell');
					icon1.attr('style', 'background-color: #C0C0C0');
					var icon3 = $('#restingCell');
					icon3.attr('style', 'background-color: #C0C0C0');
					var icon2 = $('#joggingCell');
					icon2.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-jogging').on('mouseleave', function(){
					//out of hover
					var icon = $('#walkingCell');
					icon.attr('style', 'background-color: #fff');
					var icon1 = $('#physioCell');
					icon1.attr('style', 'background-color: #fff');
					var icon3 = $('#restingCell');
					icon3.attr('style', 'background-color: #fff');
					var icon2 = $('#joggingCell');
					icon2.attr('style', 'background-color: #fff');
				});

				$('#morethan7').mouseenter(function(){
					var bar = $('.bar-resting');
					bar.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var icon = $('.bar');
					icon.attr('style', 'fill: #E6E6E6');
				});

				$('#morethan7').on('mouseleave', function(){
					//out of hover
					var bar = $('.bar-resting');
					bar.attr('style', 'fill: #669900');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var icon = $('.bar');
					icon.attr('style', 'fill: #33b5e5');
				});

				$('#lessthan3').mouseenter(function(){
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var icon = $('.bar');
					icon.attr('style', 'fill: #E6E6E6');
				});

				$('#lessthan3').on('mouseleave', function(){
					//out of hover
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #FF0000');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var icon = $('.bar');
					icon.attr('style', 'fill: #33b5e5');
				});

				$('#btw3and7').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#btw3and7').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #FF0000');
				});

				$('#walkingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#walkingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});

				$('#restingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#restingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});

				$('#joggingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#joggingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});

				$('#physioCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#physioCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});

				$('#ideal').mouseenter(function(){
					//on hover
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#ideal').on('mouseleave', function(){
					//out of hover
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});


				$('#underExert').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
				});

				$('#underExert').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
				});


				$('#overExert').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#overExert').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});

