<script>
					var tasks = [];
					var taskNames = [];
					var taskStatus = {};

					<?php echo json_encode($allPainArray);  ?>

					<?php foreach($allPainArray as $key=>$value): ?>

						<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

						<?php if($value['pain_level'] >= "7"): ?>
							taskStatus.toomuch = "bar-pain";
							var status = "toomuch";
						<?php elseif ($value['pain_level'] <= "3"): ?>
							taskStatus.average = "bar-resting";
							var status = "average";
						<?php else: ?>
							taskStatus.toomild = "bar";
							var status = "toomild";
						<?php endif ?>

						taskNames.push(<?php echo json_encode($value['day']); ?>);

					    tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode($adash); ?>,
									"status": status,
									});
				      
					<?php endforeach ?>

					taskNames.sort(function(a, b){return a-b});

						for ( x = 0; x < taskNames.length; x++){
							if(taskNames[x] == '1'){
								taskNames[x] = 'Sunday';
							} else if(taskNames[x] == '2'){
								taskNames[x] = 'Monday';
							} else if(taskNames[x] == '3'){
								taskNames[x] = 'Tuesday';
							} else if(taskNames[x] == '4'){
								taskNames[x] = 'Wednesday';
							} else if(taskNames[x] == '5'){
								taskNames[x] = 'Thursday';
							} else if(taskNames[x] == '6'){
								taskNames[x] = 'Friday';
							} else if(taskNames[x] == '7'){
								taskNames[x] = 'Saturday';
							}
						}

				tasks.sort(function(a, b) {
				    return a.endDate - b.endDate;
				});
				var maxDate = tasks[tasks.length - 1].endDate;
				tasks.sort(function(a, b) {
				    return a.startDate - b.startDate;
				});
				var minDate = tasks[0].startDate;

				var format = "%I:%M %p";

				var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
				gantt(tasks);

				

</script>

<script type="text/javascript" src="../painSeries/activityPainLegend.js"></script>