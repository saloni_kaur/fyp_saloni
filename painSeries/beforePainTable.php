<?php
    require ('../getActivitiesBeforePain.php');
    require ('../prepareActivityPainTable.php');


   
   $targetQuestion;
   $targetQuestion = "Pain occurred the most after which physical activities?";
   $tipsForPain;
?>  
<?php 

    $tipsForPain = "";
    $maxnumber = max($countPhysio, $countWalking, $countJogging, $countResting);
    
    if($maxnumber == $countPhysio) {
        $tipsForPain .= " Physiotherapy";
    }

    if($maxnumber == $countJogging) {
        if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Jogging";
    }

    if($maxnumber == $countWalking) {
        if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Walking";
    }

    if($maxnumber == $countResting){
        if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Resting";
    }   
?>
<!DOCTYPE html>
<html lang="en">
<style>
  text{
    font-size: 20px !important; 
  }
  yAxis: {
    allowDecimals: false,
    }
</style>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pain - Specific Category</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-item.css" rel="stylesheet">

     <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>


    <script src="http://d3js.org/d3.v3.min.js"></script>

    <script src="http://dimplejs.org/dist/dimple.v2.1.2.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require_once('../topNav.php'); ?>

    <div class="container">

        <?php require_once('../weekInFocus.php'); ?>  
        
        <div class="row">
            <div class="col-lg-12">
                <h3 id="questions" name="beforePainTableQuestions"><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks" name="beforePainTableRemarks"><b>Remarks:</b> <?php echo $tipsForPain; ?></h4>
                <input type="submit" name="beforePainTableSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next"/>
                        <script>
                            function sendToSummary(){

                                var question = <?php echo json_encode($targetQuestion); ?>;
                                var remarks = $('#remarks')[0].innerHTML;

                                $.post( "../forSummary.php", { beforePainTablequestionTag: question, beforePainTableremarkTag: remarks })
                                .done(function( data ) {
                                    console.log(data);
                                    data = eval("(" +data+ ")");
                                    location.replace(data['nextPage']);
                                  });
                                                                
                            }

                        </script>
            </div>
        </div>
    </div>

    <hr>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead"><u>Number of times pain occurred after activities.</u></p>
               <!--  <p class="lead">Categories</p>
                <div class="list-group">
                    <a href="../weekSpecific.php" class="list-group-item">Week -Specific</a>
                    <a href="../painSpecific.php" class="list-group-item active">Pain - Specific</a>
                    <a href="../activitySpecific.php" class="list-group-item">Activity - Specific</a>
                </div> -->
            </div>

            <div class="col-md-9">

                <div class="well">

                    

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
                    <script type="text/javascript">
                          var svg = dimple.newSvg(".well", 850, 700);
                          var data = [
                            { "Activity":"Physio", "Occurrence Last Week":<?php echo $countPhysio; ?> },
                            { "Activity":"Jog", "Occurrence Last Week":<?php echo $countJogging; ?> },
                            { "Activity":"Rest", "Occurrence Last Week":<?php echo $countResting; ?> },
                            { "Activity":"Walk", "Occurrence Last Week":<?php echo $countWalking; ?> }
                          ];
                          var chart = new dimple.chart(svg, data);
                          chart.addCategoryAxis("x", "Activity");
                          var yaxis = chart.addMeasureAxis("y", "Occurrence Last Week");
                          yaxis.showGridlines = true;
                          yaxis.tickFormat = ',.1f';
                          chart.addSeries("Activity", dimple.plot.bar);
                          chart.draw(500);
                     </script>


     <div class="container"> 

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

</body>

</html>


