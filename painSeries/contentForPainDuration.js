					var tasks = [];
					var taskNames = [];
					var taskStatus = {};

					<?php echo json_encode($allPainArray);  ?>

					<?php foreach($allPainArray as $key=>$value): ?>

						<?php if($value['pain_level'] >= "7"): ?>
							taskStatus.toomuch = "bar-pain";
							var status = "toomuch";
						<?php elseif ($value['pain_level'] <= "3"): ?>
							taskStatus.average = "bar-resting";
							var status = "average";
						<?php else: ?>
							taskStatus.toomild = "bar";
							var status = "toomild";
						<?php endif ?>

						taskNames.push(<?php echo json_encode(getDateOnly($value['start_time'])); ?>);

					    tasks.push({
					     			"startDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
					     			"endDate": new Date(<?php echo json_encode((($date1) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
									"taskName": <?php echo json_encode(getDateOnly($value['start_time'])); ?>,
									"status": status,
									});
				      
					<?php endforeach ?>

				tasks.sort(function(a, b) {
				    return a.endDate - b.endDate;
				});
				var maxDate = tasks[tasks.length - 1].endDate;
				tasks.sort(function(a, b) {
				    return a.startDate - b.startDate;
				});
				var minDate = tasks[0].startDate;

				var format = "%I:%M %p";

				var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
				gantt(tasks);

				$('.bar').mouseenter(function(){
					//on hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar').on('mouseleave', function(){
					//out of hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-resting').mouseenter(function(){
					//on hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-resting').on('mouseleave', function(){
					//out of hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-pain').mouseenter(function(){
					//on hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-pain').on('mouseleave', function(){
					//out of hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #fff');
				});
