$('.bar').mouseenter(function(){
					//on hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar').on('mouseleave', function(){
					//out of hover
					var icon = $('#btw3and7');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-resting').mouseenter(function(){
					//on hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-resting').on('mouseleave', function(){
					//out of hover
					var icon = $('#lessthan3');
					icon.attr('style', 'background-color: #fff');
				});

				$('.bar-pain').mouseenter(function(){
					//on hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #C0C0C0');
				});

				$('.bar-pain').on('mouseleave', function(){
					//out of hover
					var icon = $('#morethan7');
					icon.attr('style', 'background-color: #fff');
				});

				//////////////////////////LEGEND HOVERING//////////////////////////////
				$('#morethan7').mouseenter(function(){
					var bar = $('.bar-resting');
					bar.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var icon = $('.bar');
					icon.attr('style', 'fill: #E6E6E6');
				});

				$('#morethan7').on('mouseleave', function(){
					//out of hover
					var bar = $('.bar-resting');
					bar.attr('style', 'fill: #669900');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var icon = $('.bar');
					icon.attr('style', 'fill: #33b5e5');
				});

				$('#lessthan3').mouseenter(function(){
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var icon = $('.bar');
					icon.attr('style', 'fill: #E6E6E6');
				});

				$('#lessthan3').on('mouseleave', function(){
					//out of hover
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #FF0000');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var icon = $('.bar');
					icon.attr('style', 'fill: #33b5e5');
				});

				$('#btw3and7').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#btw3and7').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-jogging');
					bar2.attr('style', 'fill: #800080');
					var bar = $('.bar-pain');
					bar.attr('style', 'fill: #FF0000');
				});

				$('#restingCell').mouseenter(function(){
					//on hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #E6E6E6');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #E6E6E6');
					var bar = $('.bar');
					bar.attr('style', 'fill: #E6E6E6');
				});

				$('#restingCell').on('mouseleave', function(){
					//out of hover
					var icon = $('.bar-resting');
					icon.attr('style', 'fill: #669900');
					var bar2 = $('.bar-pain');
					bar2.attr('style', 'fill: #FF0000');
					var bar = $('.bar');
					bar.attr('style', 'fill: #33b5e5');
				});