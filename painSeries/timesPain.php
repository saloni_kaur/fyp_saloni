<?php
	include('../globalsVar.php');
?>
<?php
	require('../getActivitiesBeforePain.php');
	require('../prepareActivityPainTable.php');
	require('../helpers/getDateOnly.php');
	require('../activities/restingActivity.php');

	$date1 = getDateOnly($allPainArray[0]['start_time']);
	
	$date2 = getDateOnly($allRestingArray[0]['start_time']);
?>
<?php
	$targetQuestion = "What times did the elderly have pain?";
?>
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <title>Pain - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">

		<!-- <link type="text/css" href="http://mbostock.github.io/d3/style.css" rel="stylesheet" /> -->
		<link type="text/css" href="../css/example.css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src="../gantt-chart-d3.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require_once('../topNav.php'); ?>

	<div class="container">
		<?php require_once('../weekInFocus.php'); ?>  
        <div class="row">
            <div class="col-lg-12">
                <h3><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4><b>Remarks:</b> <?php echo "The average pain duration is " . round($averagePainDuration) . "mins."; ?></h4>
            	 <input type="submit" name="painDurationSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
               			<script>
							function sendToSummary(){

								var question = <?php echo json_encode($targetQuestion); ?>;
								var remarks = $('#remarks')[0].innerHTML;

								$.post( "../forSummary.php", { painDurationquestionTag: question, painDurationremarkTag: remarks })
								.done(function( data ) {
								    console.log(data);
								    data = eval("(" +data+ ")");
								    location.replace(data['nextPage']);
								  });
																
							}

						</script>
            </div>
        </div>
    </div>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	        <div class="row">

	            <div class="col-md-3">
	               <!--  <p class="lead">Categories</p>
	                <div class="list-group">
	                    <a href="../weekSpecific.php" class="list-group-item">Week -Specific</a>
	                    <a href="../painSpecific.php" class="list-group-item active">Pain - Specific</a>
	                    <a href="../activitySpecific.php" class="list-group-item">Activity - Specific</a>
	                    <a href="../illnessSpecific.php" class="list-group-item">Illness - Specific</a>
	                </div> -->

	                <p class="lead"><u>Timetable for pain only.</u></p>
	                <p class="lead">Legend</p>
	                <TABLE class="table table-bordered">
					    <TR>
					      <TH>Pain Level</TH>
					      <TH>Color Associated</TH>
					   </TR>
						<TR ALIGN="LEFT" id="morethan7">
					      <TD>More than 7</TD>
					      <TD style="background-color:#FF0000;">
						      <div></div>
					      </TD>
					    </TR>
					     <TR ALIGN="LEFT" id="btw3and7">
					      <TD>Between 3 to 7</TD>
					      <TD style="background-color:#33b5e5;">
						      <div> </div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="lessthan3">
					      <TD>Less than 3</TD>
					      <TD style="background-color:#669900;">
						      <div> </div>
					      </TD>
					    </TR>
					</TABLE>
					<?php require('../legendInstruction.php'); ?>
	            </div>

	            <div class="col-md-9">

	                <div class="well">
	                	<?php require('contentForPainDuration.php'); ?>
	                    
	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->
		    

    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>

