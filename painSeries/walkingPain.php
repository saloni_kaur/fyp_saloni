<?php
	include('../globalsVar.php');
?>
<?php
	require('../getActivitiesBeforePain.php');
	require('../prepareActivityPainTable.php');
	require('../helpers/getDateOnly.php');
	require('../activities/walkingActivity.php');

	$date1 = getDateOnly($allPainArray[0]['start_time']);
	$date2 = getDateOnly($allWalkingArray[0]['start_time']);
?>
<?php
	$targetQuestion = "Did walking cause a pain level of more than 7?";
?>
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">


		<title>Pain - Specific</title>

		 <!-- Bootstrap Core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../css/shop-item.css" rel="stylesheet">

		<!-- <link type="text/css" href="http://mbostock.github.io/d3/style.css" rel="stylesheet" /> -->
		<link type="text/css" href="../css/example.css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src="../gantt-chart-d3.js"></script>
		<script type="text/javascript" src="jsFunctions/jsFunctionsPainSeries.js"></script>

		 <!-- jQuery -->
	    <script src="../js/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php require_once('../topNav.php'); ?>

	<div class="container">
		<?php require_once('../weekInFocus.php'); ?>  
        <div class="row">
            <div class="col-lg-12">
                <h3 id="questions"><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks"></h4>
                <input type="submit" name="walkingPainSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
               			<script>
							function sendToSummary(){

								var question = <?php echo json_encode($targetQuestion); ?>;
								var remarks = $('#remarks')[0].innerHTML;

								$.post( "../forSummary.php", { walkingPainquestionTag: question, walkingPainremarkTag: remarks })
								.done(function( data ) {
								    console.log(data);
								    data = eval("(" +data+ ")");
								    location.replace(data['nextPage']);
								  });
																
							}

						</script>
            </div>
        </div>
    </div>


	</script>

    <hr>

	     <!-- Page Content -->
	    <div class="container">

	        <div class="row">

	            <div class="col-md-3">
	        <!--         <p class="lead">Categories</p>
	                <div class="list-group">
	                    <a href="../weekSpecific.php" class="list-group-item">Week -Specific</a>
	                    <a href="../painSpecific.php" class="list-group-item active">Pain - Specific</a>
	                    <a href="../activitySpecific.php" class="list-group-item">Activity - Specific</a>
	                  
	                </div> -->

	                <p class="lead"><u>Timetable for Walking and Pain periods only.</u></p>
	                <p class="lead">Legend</p>
	                <TABLE class="table table-bordered">
					    <TR>
					      <TH>Pain Level</TH>
					      <TH>Color Associated</TH>
					   </TR>
						<TR ALIGN="LEFT"  id="lessthan3">
					      <TD>&lt; 3</TD>
					      <TD style="background-color:#669900;">
						      <div> </div>
					      </TD>
					    </TR>
						
					    <TR ALIGN="LEFT" id="btw3and7">
					      <TD> 3 to 7</TD>
					      <TD style="background-color:#33b5e5;">
						      <div> </div>
					      </TD>
					    </TR>
					    <TR ALIGN="LEFT" id="morethan7">
					      <TD>&gt; 7</TD>
					      <TD style="background-color:#FF0000;">
						      <div></div>
					      </TD>
					    </TR>
					    </TABLE>
					    <TABLE class="table table-bordered">
					   <TR>
					      <TH>Activity</TH>
					      <TH>Color Associated</TH>
					   </TR>
					    <TR ALIGN="LEFT" id="walkingCell">
					      <TD>Walking</TD>
					      <TD style="background-color:#800080">
						      <div> </div>
					      </TD>
					    </TR>
					</TABLE>
				<?php require('../legendInstruction.php'); ?>

	            </div>

	            <div class="col-md-9">

	                <div class="well">
						<script>
								var tasks = [];
								var taskNames = [];
								<?php foreach($allWalkingArray as $key=>$value): ?>

									<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

									taskNames.push(<?php echo ($value['day']); ?>);

								    tasks.push({
								     			"startDate": new Date(<?php echo json_encode((($date2) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
								     			"endDate": new Date(<?php echo json_encode((($date2) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
												"taskName": <?php echo json_encode($adash); ?>,
												"status": "walking",
												});
								      
								<?php endforeach ?>

								<?php foreach($allPainArray as $key=>$value): ?>

									<?php switch ($value['day']) {
										case '1' :	$adash = 'Sunday'; break;
										case '2' :	$adash = 'Monday'; break;
										case '3' :	$adash = 'Tuesday'; break;
										case '4' :	$adash = 'Wednesday'; break;
										case '5' :	$adash = 'Thursday'; break;
										case '6' :	$adash = 'Friday'; break;
										case '7' :	$adash = 'Saturday'; break;
									} ?>

									<?php if($value['pain_level'] >= "7"): ?>
										var status = "toomuch";
									<?php elseif ($value['pain_level'] <= "3"): ?>
										var status = "average";
									<?php else: ?>
										var status = "toomild";
									<?php endif ?>

									taskNames.push(<?php echo ($value['day']); ?>);

								    tasks.push({
								     			"startDate": new Date(<?php echo json_encode((($date2) . ' ' . (explode(" ", $value['start_time'])[1]))); ?>),
								     			"endDate": new Date(<?php echo json_encode((($date2) . ' ' . (explode(" ", $value['end_time'])[1]))); ?>),
												"taskName": <?php echo json_encode($adash); ?>,
												"status": status,
												});
				      
								<?php endforeach ?>

						var taskStatus = {
						    "walking" : "bar-jogging",
						    "toomuch" : "bar-pain",
						    "toomild" : "bar",
						    "average" : "bar-resting"
						};

						taskNames.sort(function(a, b){return a-b});

						for ( x = 0; x < taskNames.length; x++){
							if(taskNames[x] == '1'){
								taskNames[x] = 'Sunday';
							} else if(taskNames[x] == '2'){
								taskNames[x] = 'Monday';
							} else if(taskNames[x] == '3'){
								taskNames[x] = 'Tuesday';
							} else if(taskNames[x] == '4'){
								taskNames[x] = 'Wednesday';
							} else if(taskNames[x] == '5'){
								taskNames[x] = 'Thursday';
							} else if(taskNames[x] == '6'){
								taskNames[x] = 'Friday';
							} else if(taskNames[x] == '7'){
								taskNames[x] = 'Saturday';
							}
						}

							var countWalkPain = evaluateTipsWalkPain(tasks);
								if(countWalkPain > 1){
									var remarksEle = document.getElementById('remarks');
									remarksEle.innerHTML = "Remarks: Yes. After walking, pain level was more than 7.";
								} else {
									var remarksEle = document.getElementById('remarks');
									remarksEle.innerHTML = "Remarks: No. After walking, there was pain but it was not too much.";
								}

							tasks.sort(function(a, b) {
							    return a.endDate - b.endDate;
							});
							var maxDate = tasks[tasks.length - 1].endDate;
							tasks.sort(function(a, b) {
							    return a.startDate - b.startDate;
							});
							var minDate = tasks[0].startDate;

							var format = "%I:%M %p";

							var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
							gantt(tasks);
						</script>
	                    
		<script type="text/javascript" src="activityPainLegend.js"></script>


	                </div>

	            </div>

	        </div>

	    </div>
	    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->


</body>


</html>