<?php
    require ('../getActivitiesBeforePain.php');
    require ('../prepareActivityPainTable.php');

   global $targetQuestion;
   $targetQuestion = "The total pain level (all ranges) was the highest after which physical activities?";
   $tipsForPain = "";
?>  
<?php 

    $maxnumber = max($resultantArray);

    $countPhysio = 0;
    $countRest = 0;
    $countWalk = 0;
    $countJog = 0;

    foreach ($resultantArray as $key => $value) {
        if($value == $maxnumber){
            if($key == "Physiotherapy"){
                $countPhysio = 1;
            } else if ($key == "Jogging"){
                $countJog = 1;
            } else if($key == "Walking"){
                $countWalk = 1;
            } else {
                $countRest = 1;
            }
        }
    }
    
    if($countPhysio == 1) {
        $tipsForPain = "";
        $tipsForPain .= " Physiotherapy";
    }

    if($countJog == 1) {
       if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Jogging";
    }

    if($countWalk == 1) {
        if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Walking";
    }

    if($countRest == 1){
        if($tipsForPain != ""){
            $tipsForPain .= " and";
        }
        $tipsForPain .= " Resting";
    }
    
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pain - Specific Category</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-item.css" rel="stylesheet">

     <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require_once('../topNav.php'); ?>

     <div class="container">

        <?php require_once('../weekInFocus.php'); ?>  

        <div class="row">
            <div class="col-lg-12">
                <h3><b>Target Question:</b> <?php echo $targetQuestion; ?></h3>
                <h4 id="remarks"><b>Remarks:</b> <?php echo $tipsForPain; ?></h4>
                <input type="submit" name="beforePainLevelSubmit" class="btn btn-primary btn-large" onclick="sendToSummary()" value="Next" style="float:right"/>
                        <script>
                            function sendToSummary(){

                                var question = <?php echo json_encode($targetQuestion); ?>;
                                var remarks = $('#remarks')[0].innerHTML;

                                $.post( "../forSummary.php", { beforePainLevelquestionTag: question, beforePainLevelremarkTag: remarks })
                                .done(function( data ) {
                                    console.log(data);
                                    data = eval("(" +data+ ")");
                                    location.replace(data['nextPage']);
                                  });
                            }

                        </script>
            </div>
        </div>
    </div>

    <hr>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead"><u>Total pain level after each activity.</u></p>
            </div>

            <div class="col-md-9">

                <div class="well">
                    <TABLE class="table table-bordered">
                       <TR>
                          <TH>Activity Before Pain</TH>
                          <TH>Pain Level</TH>
                       </TR>
                       <?php 
                            $level = array();
                            foreach ($resultantArray as $key => $value)
                            {
                                $level[$key] = $value;
                            }
                            array_multisort($level, SORT_DESC, $resultantArray);
                       ?>
                       <?php foreach($resultantArray as $key=>$value): ?>
                           <TR ALIGN="LEFT">
                              <TD><?php echo $key; ?></TD>
                              <TD>
                                  <?php for($x = 0; $x < $value; $x++): ?>
                                    <img src="../images/painface.jpg" alt="Pain face" height="42" width="42">
                                  <?php endfor ?>
                              </TD>
                           </TR>
                        <?php endforeach ?>
                    </TABLE>
                    

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->



     <div class="container"> 

        <hr>

        <!-- Footer -->
       <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

</body>

</html>


