<?php
    require('forSummary.php');
?>
<?php
$link = mysqli_connect('localhost', 'root', 'silversense123');
mysqli_select_db($link,'silversense');
// Evaluate the connection
if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
    exit();
} else {
    //echo "Successful database connection, happy coding!!!";
}

$sqlQuery = mysqli_query($link, "SELECT question, remark FROM report WHERE elderly_id=1 AND user_id=1");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DrVisual</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require_once('topNav-root.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

                <div class="col-md-3">
                    <p class="lead">Categories</p>
                    <div class="list-group">
                        <a href="weekSpecific.php" class="list-group-item">Week -Specific</a>
                        <a href="painSpecific.php" class="list-group-item">Pain - Specific</a>
                        <a href="activitySpecific.php" class="list-group-item">Activity - Specific</a>
                        <!-- <a href="illnessSpecific.php" class="list-group-item">Illness - Specific</a> -->
                    </div>
                </div>


         <!-- Title -->
                <div class="col-lg-9">
                    <h3><u>Report Card</u></h3>
                    </br>
                    </br>
                    </br>
                    </br>
                    <?php
                        $rowCount2 = mysqli_num_rows($sqlQuery);
                        if($rowCount2 > 0){
                            while($row2 = mysqli_fetch_array($sqlQuery, MYSQLI_ASSOC)){ ?>
                                <h4><b> <?php echo $row2['question'] . "</br>" ?></b></h4>
                                <h4><i> <?php echo $row2['remark'] . "</br></br></br>" ?></i></h4>
                            <?php }
                        } else { ?>
                            <h4><b> <?php echo "No questions viewed yet."; ?> </b></h4>
                        <?php } ?>
                    </br>
                    </br>
                    </br>
                    </br>
                    <a href="index.php"><input type="submit" class="btn btn-primary btn-large" value="Go To Homepage"/></a>
                </div>
            </div>
            <!-- /.row -->
    </div>


</body>

</html>