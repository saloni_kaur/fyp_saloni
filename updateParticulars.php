<?php
	 include ('smartFilter.php');
	 include ('getElderlyParticulars.php');

	 $name = $particularsArray[0]['name'];
	 $age = $particularsArray[0]['age'];
	 $idealEnergyValue = $particularsArray[0]['idealEnergyValue'];
?>

<head>
	<link type="text/css" href="css/updateParticulars.css" rel="stylesheet" />
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
	<img id="logo" src="images/DrVisualLogo.png" alt="Logo" height="50">
	<div id="updateDiv">
		<b>Name:</b><div id="name"><?php echo $name; ?></div>
		<b>Age:</b> <div id="age"><?php echo $age; ?></div>
		<b>Exercise Ideal Energy:</b> <input type="text" id="energyValue" value=<?php echo json_encode($idealEnergyValue); ?>/>
		<input id="saveBtn" type="button" onclick="saveNewEnergyIdeal()" value= "Save"/>
	</div>
</body>

<script>
	function saveNewEnergyIdeal(){

		var newIdealValue = $('#energyValue')[0].value;

		var posting = $.post( "saveIdealEnergyValue.php", { newIdeal: newIdealValue }, function( data ) {
		  	alert('New Ideal Energy Value Saved!');
		    $( "#energyValue" ).value = data;
		});
	}
</script>