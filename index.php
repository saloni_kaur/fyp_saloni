<?php
$link = mysqli_connect('localhost', 'root', 'silversense123');
mysqli_select_db($link,'silversense');
// Evaluate the connection
if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
    exit();
} else {
    //echo "Successful database connection, happy coding!!!";
}
$sqlQuery = mysqli_query($link, "DELETE FROM report WHERE 1");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DrVisual</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php require_once('topNav-root.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h1>DrVisual Welcomes You!</h1>
            <p>Get the answers to most of your questions regarding your elderly. There is now no need to rely on elderly's memory. Product of SilverSense.</p>
            <p><a class="btn btn-primary btn-large" href="autocomplete.php">Search for Question!</a>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Categories of Questions</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="images/weekIcon.png" style="width: 50%" alt="">
                    <div class="caption">
                        <h3>Week - Specific</h3>
                        <p>Questions which are based on the routine of the week.</p>
                        <p>
                            <a href="weekSpecific.php" class="btn btn-primary">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="images/painIcon.png" style="width: 50%" alt="">
                    <div class="caption">
                        <h3>Pain - Specific</h3>
                        <p>Questions which are based on pain.</p>
                        <p>
                            <a href="painSpecific.php" class="btn btn-primary">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="images/activityIcon.png" style="width: 50%"  alt="">
                    <div>Icons made by <a href="http://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
                    <div class="caption">
                        <h3>Activity - Specific</h3>
                        <p>Questions which are based on a certain activity.</p>
                        <p>
                            <a href="activitySpecific.php" class="btn btn-primary">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

<!--             <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="images/illnessIcon.png" style="width: 50%"  alt="">
                    <div class="caption">
                        <h3>Illness - Specific</h3>
                        <p>Questions which are based on a certain illness.</p>
                        <p>
                            <a href="illnessSpecific.php" class="btn btn-primary">More Info</a>
                        </p>
                    </div>
                </div>
            </div> -->

        </div>
        <!-- /.row -->



        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; DrVisual 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
