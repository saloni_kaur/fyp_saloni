<?php
$link = mysqli_connect('localhost', 'root', 'silversense123');
mysqli_select_db($link,'silversense');
// Evaluate the connection
if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
    exit();
} else {
	//echo "Successful database connection, happy coding!!!";
}
?>
<?php

include_once ('../helpers/calculateTotalTime.php');

$allRestingArray = array();
$sqlQuery3 = mysqli_query($link, "SELECT start_time, end_time, day FROM activities WHERE description = 'resting' AND start_time >= '2014-01-26 00:00:00' ORDER BY start_time");

$rowCount3 = mysqli_num_rows($sqlQuery3);

if ($rowCount3 > 0){
	while($row3 = mysqli_fetch_array($sqlQuery3, MYSQLI_ASSOC)){
		array_push($allRestingArray, $row3);
	}

	//FOR TESTING PURPOSE
	//print json_encode($allRestingArray);

	//parseStartDate($allRestingArray);
	$totalTime3 = calculateTotalTime($allRestingArray);
	$averageRestDuration = calculateAvgTime($allRestingArray);
}

?>