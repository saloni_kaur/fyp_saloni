<?php
$link = mysqli_connect('localhost', 'root', 'silversense123');
mysqli_select_db($link,'silversense');
// Evaluate the connection
if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
    exit();
} else {
	//echo "Successful database connection, happy coding!!!";
}
?>
<?php

include_once ('../helpers/calculateTotalTime.php');

$allPainArray = array();
$sqlQuery4 = mysqli_query($link, "SELECT start_time, end_time, pain_level, day, painLocation FROM activities WHERE description = 'pain' AND start_time >= '2014-01-26 00:00:00' ORDER BY start_time");

$rowCount4 = mysqli_num_rows($sqlQuery4);

if ($rowCount4 > 0){
	while($row4 = mysqli_fetch_array($sqlQuery4, MYSQLI_ASSOC)){
		array_push($allPainArray, $row4);
	}
	
	$totalTime4 = calculateTotalTime($allPainArray);
	$totalPain = calculateTotalPain($allPainArray);
	$avgPain = ($totalPain / $rowCount4);
	$datesBeforePain = datesWithPain($allPainArray);
	$painLevelArray = painLevelArray($allPainArray);
	$averagePainDuration = calculateAvgTime($allPainArray);

}

function calculateTotalPain($allArray){
	$count = 0;
	foreach ($allArray as $j) {
		$count += $j['pain_level'];
	}
	return $count;
}

function datesWithPain($allArray){
	$datesArray = array();
	foreach($allArray as $z){
		array_push($datesArray, $z['start_time']);
	}
	return $datesArray;
}

function painLevelArray($allArray){
	$painLevelArray = array();
	foreach ($allArray as $i) {
		array_push($painLevelArray, $i['pain_level']);
	}
	return $painLevelArray;
}

?>