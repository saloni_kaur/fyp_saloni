<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DrVisual</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <style>
  .ui-autocomplete-category {
    font-weight: bold;
    padding: .2em .4em;
    margin: .8em 0 .2em;
    line-height: 1.5;
  }
  </style>
  <script>
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
        if ( item.category ) {
          li.attr( "aria-label", item.category + " : " + item.label );
        }
      });
    }
  });
  </script>
  <script>
  $(function() {
    var data = [
      { label: "What was the physical activities routine like for the elderly last week?", category: "Week-Specific", id:"1"},
      { label: "Pain occurred the most after which physical activities?", category: "Pain-Specific", id:"2" },
      { label: "The total pain level (all ranges) was the highest after which physical activities?", category: "Pain-Specific", id:"3" },
      { label: "What was the average duration of the pain?", category: "Pain-Specific", id:"4" },
      { label: "Did resting cause too much increase in pain?", category: "Pain-Specific", id:"5" },
      { label: "Did walking cause too much increase in pain?", category: "Pain-Specific", id:"6" },
      { label: "Did jogging cause too much increase in pain?", category: "Pain-Specific", id:"7" },
      { label: "Did physiotherapy cause too much increase in pain?", category: "Pain-Specific", id:"8" },
      { label: "What times did the elderly jog?", category: "Activity-Specific", id:"9" },
      { label: "What times did the elderly walk?", category: "Activity-Specific", id:"10" },
      { label: "What times did the elderly rest?", category: "Activity-Specific", id:"11" },
      { label: "What times did the elderly physiotherapy?", category: "Activity-Specific", id:"12" },
      { label: "What times did the elderly pain?", category: "Activity-Specific", id:"13" },
      { label: "Is the physiotherapy exercise done with the correct intensity?", category: "Activity-Specific", id:"14" },
      { label: "Which was the major activity?", category: "Activity-Specific", id:"15" },
      { label: "What is the difference in activities duration in the last 2 weeks?", category: "Activity-Specific", id:"16" }

    ];
 
    $( "#search" ).catcomplete({
      delay: 0,
      source: data,
      select: function(event, ui) {
                $('#searchval').val(ui.item.id);
            }
    });
  });
  </script>
<style>
  label{
    font-size: large !important;
  }
  #search{
      width: 50% !important;
  }
</style>

</head>
  
<body>
   <?php require('topNav-root.php'); ?>

    <!-- Page Content -->
    <div class="container">
        <!-- Title -->
        <div class="row text-center">
            <div class="col-lg-12">
                <label for="search">Search for Question: </label>
                <input id="search">
                 <input id="searchval" type="hidden"/>
                <a class="btn btn-primary btn-large" onclick="goToPage()">Ask!</a>
            </div>
        </div>
        <!-- /.row -->
    </div>
 

 
 
</body>
</html>

<script>
function goToPage(){
  var itemID = $("#searchval")[0].value;
  console.log(itemID);
  switch(itemID) {
    case "1":
        location.replace('weekSeries/lastWeek.php');
        break;
    case "2":
       location.replace('painSeries/beforePainTable.php');
       break;
    case "3":
       location.replace('painSeries/beforePainLevelTable.php');
       break;
    case "4":
       location.replace('painSeries/painDuration.php');
       break;
    case "5":
       location.replace('painSeries/restingPain.php');
       break;
    case "6":
       location.replace('painSeries/walkingPain.php');
       break;
    case "7":
       location.replace('painSeries/joggingPain.php');
       break;
    case "8":
       location.replace('painSeries/physioPain.php');
       break;
    case "9":
       location.replace('activitySeries/joggingDuration.php');
       break;
    case "10":
       location.replace('activitySeries/walkingDuration.php');
       break;
    case "11":
       location.replace('activitySeries/restingDuration.php');
       break;
    case "12":
       location.replace('activitySeries/timesPhysio.php');
       break;
    case "13":
       location.replace('activitySeries/timesPain.php');
       break;
    case "14":
       location.replace('activitySeries/physioIntensityTable.php');
       break;
    case "15":
       location.replace('activitySeries/majorActivity.php');
       break;
    case "16":
       location.replace('activitySeries/majorActivityCompare.php');
       break;    
    default:
       console.log(itemID);
  }

}


</script>

